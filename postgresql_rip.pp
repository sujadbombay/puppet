class sentifi_postgresql::postgresql_rip (
  $pguser = "postgres",
  $pggroup = "postgres",
  $mount_point_sdc = "/data/postgresql/sns_account",
  $mount_point_sdd = "/data/postgresql/sns_account_audit",
  Array $ddllogroles = [ 'pnotification', 'pprofileservice', 'prankprofilecrawler', 'ptopicservice' ],
) {
  sentifi_disk {"pgsql_sdc":
    device      => "sdc",
    mount_point => $mount_point_sdc,
  }->
  sentifi_disk {"pgsql_sdd":
    device      => "sdd",
    mount_point => $mount_point_sdd,
  }->
  file {["$mount_point_sdc", "$mount_point_sdd"]:
    ensure => directory,
    owner  => $pguser,
    group  => $pggroup,
  }->
  sentifi_postgresql::init {"postgresql_rip":
    databases => [ 'postgres', 'rest_in_peace' ],
  }
  $ddllogroles.each |String $ddllogrole| {

    exec {"Set log level ddl for user ${ddllogrole}":
      require  => Sentifi_postgresql::Init["postgresql_rip"],
      command  => "psql -c \'ALTER ROLE ${ddllogrole} SET log_statement = ddl\'",
      user     => $pguser,
      provider => "shell",
      cwd      => "/var/lib/pgsql",
      unless   => "psql -c \"select rolconfig from pg_roles where rolname = \'${ddllogrole}\'\" | grep \'log_statement=ddl\'",
    }
  }
#  sentifi_postgresql::postgresql_users {"dbo":
#    pguser_pw                  => "sentifi",
#  }
#  sentifi_postgresql::postgresql_users {"dbr":
#    pguser_pw                  => "sentifi",
#  }
#  sentifi_postgresql::postgresql_users {"dbw":
#    pguser_pw                  => "sentifi",
#  }
#  sentifi_postgresql::postgresql_users {"root":
#    pguser_pw                  => "GI34DI4f",
#  }
}

class sentifi_postgresql::postgresql_rip::int_fo (
#  $wale_gs_prefix = "gs://int-fo-pgsql-rip/wal-e/",
#  $google_service_account_key = "sentifi-frontoffice-int-a252fdf75615.json",
) inherits sentifi_postgresql::postgresql_rip {
  Sentifi_postgresql::Init["postgresql_rip"]{
    shared_buffers       => "7GB",  # 1/2 of total RAM
    effective_cache_size => "25GB",  # 1/2 of total RAM
    env                  => "int_fo",
  }->
  cron {"Vacuum table sns_account":
    ensure  => present,
    command => "psql rest_in_peace -c \'vacuum analyze sns_account\'",
    user    => $pguser,
    hour    => "*/3",
    minute  =>  "0",
  }
#  sentifi_postgresql::wale {"postgresql_rip":
#    wale_gs_prefix             => $wale_gs_prefix,
#    google_service_account_key => $google_service_account_key,
#  }
}

class sentifi_postgresql::postgresql_rip::qa_bo inherits sentifi_postgresql::postgresql_rip {
  Sentifi_postgresql::Init["postgresql_rip"]{
    shared_buffers       => "16GB",  # 1/2 of total RAM
    effective_cache_size => "50GB",  # 1/2 of total RAM
    max_wal_size         => "1024",
    env                  => "qa_bo",
  }->
  cron {"Vacuum table sns_account":
    ensure  => present,
    command => "psql rest_in_peace -c \'vacuum analyze sns_account\'",
    user    => $pguser,
    hour    => "*/3",
    minute  => "0",
  }->
  cron {"Sync data from Prod to QA and INT":
    ensure  => present,
    command => "/data/postgresql/talend/script/run-sync-data-yesterday.qa-to-int.sh >> /var/lib/pgsql/backup/backup_logs/daily_backup.qa-to-int.log 2>&1",
    hour    => "1",
    minute  => "0",
    user    => $pguser,
  }
}

class sentifi_postgresql::postgresql_rip::prod_bo_01 (
  $package_name = "postgresql_rip",
  $module = "sentifi_postgresql",
  $pguser = "postgres",
  $pggroup = "postgres",
  $pgver = "9.6",
  $pgcluster = "prod_bo_rip",
  $pgnode_no = "1",
  $pgnode_name = "prod_bo_rip_01",
  $wale_gs_prefix = "gs://prod-bo-pgsql-rip/wal-e",
  $google_service_account_key = "sentifi-backoffice-prod-38592bb643dc.json",
) inherits sentifi_postgresql::postgresql_rip {
  Sentifi_postgresql::Init["postgresql_rip"]{
    shared_buffers       => "35GB",  # 1/2 of total RAM
    effective_cache_size => "80GB",  # 1/2 of total RAM
    repl                 => true,
    env                  => "prod_bo",
  }->
  file {"/etc/repmgr/$pgver/repmgr.conf":
    ensure  => file,
    content => template("$module/$package_name/repmgr_$pgnode_name.conf.erb"),
    backup  => true,
  }->
  exec {"Alter user repmgr's search_path":
    command  => "psql -c \'ALTER USER repmgr SET search_path TO repmgr_$pgcluster, \"\$user\", public\'",
    user     => $pguser,
    provider => "shell",
    cwd      => "/var/lib/pgsql",
    unless   => "psql -c \"SELECT setconfig FROM pg_db_role_setting WHERE setrole = (select oid from pg_roles where rolname = \'repmgr\' limit 1)\" | grep \'search_path=repmgr_prod_bo_rip, \\\\\"\$user\\\\\", public\'",
  }->
  cron {"Vacuum table sns_account":
    ensure  => present,
    command => "psql rest_in_peace -c \'vacuum analyze sns_account\'",
    user    => $pguser,
    hour    => "*/3",
    minute  => "0",
  }->
  file {"/data/scripts/prod_rip_google_snapshots_daily.sh":
    ensure => file,
    source => "puppet:///modules/$module/$package_name/prod_rip_google_snapshots_daily.sh",
    owner  => root,
    group  => root,
  }->
  cron {"Daily backup using Google Disk Snapshot":
    ensure  => present,
    command => "sh /data/scripts/prod_rip_google_snapshots_daily.sh > /data/scripts/logs/prod_rip_google_snapshots_daily_`date '+\%y\%m\%d-\%H\%M'`.log 2>&1",
    user    => $pguser,
    hour    => "19",
    minute  => "01",
  }->
  sentifi_postgresql::wale {"postgresql_rip":
    wale_gs_prefix             => $wale_gs_prefix,
    google_service_account_key => $google_service_account_key,
  }
}

class sentifi_postgresql::postgresql_rip::prod_bo_02 (
  $package_name = "postgresql_rip",
  $module = "sentifi_postgresql",
  $pguser = "postgres",
  $pggroup = "postgres",
  $pgver = "9.6",
  $pgcluster = "prod_bo_rip",
  $pgnode_no = "2",
  $pgnode_name = "prod_bo_rip_02",
  $mount_point_sdc = "/data/postgresql/sns_account",
  $mount_point_sdd = "/data/postgresql/sns_account_audit",
  $wale_gs_prefix = "gs://prod-bo-pgsql-rip/wal-e",
  $google_service_account_key = "sentifi-backoffice-prod-38592bb643dc.json",
) inherits sentifi_postgresql::postgresql_rip {
  Sentifi_postgresql::Init["postgresql_rip"]{
    shared_buffers       => "40GB",  # 1/2 of total RAM
    effective_cache_size => "80GB",  # 1/2 of total RAM
    repl                 => true,
    slave                => true,
    env                  => "prod_bo",
  }->
  file {"/etc/repmgr/$pgver/repmgr.conf":
    ensure  => file,
    content => template("$module/$package_name/repmgr_$pgnode_name.conf.erb"),
    backup  => true,
  }->
  cron {"Daily Backup databasechangelog table":
    ensure  => present,
    command => "/var/lib/pgsql/scripts/luan.nguyen/backup.liquibase.sh",
    hour    => "21",
    minute  => "0",
    user    => $pguser,
  }->
  cron {"Sync data from Prod to QA and INT":
    ensure  => present,
    command => "/data/postgresql/talend/script/run-sync-data-yesterday.sh >> /var/lib/pgsql/backup/backup_logs/daily_backup.log 2>&1",
    hour    => "22",
    minute  => "0",
    weekday => "7",
    user    => $pguser,
  }->
  sentifi_postgresql::wale {"postgresql_rip":
    wale_gs_prefix             => $wale_gs_prefix,
    google_service_account_key => $google_service_account_key,
  }
}

