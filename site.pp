hiera_include('classes')
node default {
}

#----PROD NODES----
node /^temp-widgetapitest/ {
	include tuning
}

node /^prod-loadtest/ {
  include tuning
}

node /^prod-puppet-01/ {
  include tuning
}

node /^vpn-gce-dns/ {
  include sentifi_openvpn::gce
  include sentifi_dns
  class {"tuning":
    iptables   => true,
    ip_forward =>  true,
  }
}

node /^dns/ {
  include sentifi_dns
  class {"tuning":
    iptables   => true,
    ip_forward => true,
  }
}


node /^gocd-server/ {
  include tuning
  include sentifi_gocd::server
}

node /^prod-gocd/ {
  include sentifi_gocd_new::prod
  include tuning
}

node /^qa-gocd/ {
  include sentifi_gocd_new::qa
  include tuning
}

node /^jfrog-artifactory/ {
  include sentifi_jfrog_artifactory
}

node /^prod-yum-repository/ {
  include sentifi_yum_repo
  include sentifi_configuration_service
  include tuning
}

node /^prod-widget-monitor-hub-01/ {
  include tuning
  include sentifi_widget_monitor::hub_controller
  include sentifi_widget_monitor::widget_script
  include stackdriver::logging::widget_monitor
}

node /^prod-widget-monitor-node/ {
  include tuning
  include sentifi_widget_monitor::node
}

node /^prod-widget-ui-automation/ {
  include sentifi_widget_ui_automation
}

node /^prod-sonarqube/ {
	include tuning
}

node /^prod-elk/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_6::prod_elk
  include sentifi_kibana_6::prod_elk
  include sentifi_logstash_6::prod_elk
  include sentifi_curator_5::prod_elk
}
#----PROD ANALYTICS-----
node /^piwik-sql-proxy/ {
  include sentifi_cloud_sql_proxy::piwik
  include tuning
}
node /^piwik-redis-01/ {
  include sentifi_redis::redis_piwik::prod_analytics
}

node /^piwik-prod-redis-01/ {
  include sentifi_redis::redis_piwik::prod_analytics
}

#node /^piwik-prod-archive-and-report-instance/ {
#  include sentifi_piwik::setup_archive_and_report_instance
#}

#node /^piwik-prod-api-instance-group/ {
#  include tuning
#  include sentifi_piwik::setup_api_instance
#}

#----PROD BO NODES-----
node /^prod-bo-nginx/ {
  include tuning
  include sentifi_nginx::prod_bo
  include sentifi_kibana::prod_bo
}

node /^prod-bo-ontology-proxy/ {
  include tuning
}

node /^prod-bo-semantic/ {
  include tuning
}

node /^prod-bo-new-ext-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::prod_bo_new_ext::master
}

node /^prod-bo-new-ext-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::prod_bo_new_ext::data
}

node /^prod-bo-new-ext-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::prod_bo_new_ext::client
}

node /^prod-bo-sim-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::prod_bo_sim::master
}

node /^prod-bo-sim-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::prod_bo_sim::data
}

node /^prod-bo-sim-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::prod_bo_sim::client
}

node /^prod-bo-redis-com/ {
  include sentifi_redis::redis_com::prod_bo
}


node /^prod-bo-redis-streaming-01/ {
  include sentifi_redis::redis_streaming_new_1::prod_bo
}

node /^prod-bo-redis-sim/ {
  include sentifi_redis::redis_sim::prod_bo
}

node /^prod-bo-ranking/ {
  include sentifi_redis::redis_ranking::prod_bo
}

node /^prod-bo-pgsql-pgpool/ {
  include tuning
  include sentifi_pgpool::prod_bo
}

node /^prod-bo-pgsql-rip-01/ {
  include tuning
  include sentifi_postgresql::postgresql_rip::prod_bo_01
}

node /^prod-bo-pgsql-rip-02/ {
  include tuning
  include sentifi_postgresql::postgresql_rip::prod_bo_02
}

node /^prod-bo-pgsql-rip-03/ {
  include tuning
  include sentifi_postgresql::postgresql_rip::prod_bo_03
}

node /^prod-bo-pgsql-da0/ {
  include tuning
  include sentifi_postgresql::postgresql_da0::prod_bo
}

node /^prod-bo-mongodb-raw/ {
  include sentifi_mongodb::mongodb_raw::prod_bo
}

node /^prod-bo-mongodb-relevant/ {
  include sentifi_mongodb::mongodb_relevant::prod_bo
}

node /^prod-bo-mongodb-ranking/ {
  include sentifi_mongodb::mongodb_ranking::prod_bo
}

node /^prod-bo-rabbitmq/ {
  include tuning
  include sentifi_rabbitmq::prod_bo
}

node /^prod-bo-topic-service/ {
  $db_url = $prod_rip_url
  $db_username = "ptopicservice"
  $db_password = "&@k#sPoeB"
  $elasticsearch_cluster_name = $qa_es_cluster
  $elasticsearch_cluster_nodes = $qa_es_host
  $gcloud_pubsub_project = "sentifi-backoffice-prod"
  $gcloud_pubsub_topic = "topic-cdc"

  include tuning
  include sentifi_topic_service::prod_bo
}

node /^prod-bo-profile-service/ {
  $db_url = $prod_rip_url
  $db_username = "pprofileservice"
  $db_password = "Z>[^]2#ik"
  $gcloud_pubsub_project = "sentifi-backoffice-prod"
  $gcloud_pubsub_topic = "profile-cdc"

  include tuning
  include sentifi_profile_service::prod_bo
}

node /^prod-bo-stock-price/ {
  $db_url = $prod_rip_url
  $db_username = "pstockpricefetch"
  $db_password = "*)hatQkD@"
  $topic_service_url = $prod_topic_service_url
  include tuning
  include sentifi_stock_price::prod_bo
}

node /^prod-bo-data-management-portal/ {
  $clientId = $google_oauth2_client_id
  $clientSecret = $google_oauth2_client_secret
  $db_url = $prod_rip_url
  $db_username = "pportalmgmt"
  $db_password = "5Z5S*1>{!"
  $topic_service_url = $prod_topic_service_url
  $profile_service_url = $prod_profile_service_url
  $version = $data_management_portal_prod_bo_version
  $elasticsearch_cluster_name = $qa_es_cluster
  $elasticsearch_cluster_nodes = $qa_es_host

  include sentifi_data_management_portal::prod_bo
}

node /^prod-bo-internal-tools/ {
  include sentifi_internal_tools::prod_bo
}

node /^streaming-swarm-node/ {
  include stackdriver::logging::streaming_docker_container
}

#----QA BO NODES----
node /^qa-bo-ontology-proxy/ {
  include tuning
}

node /^qa-bo-redis-com/ {
  include sentifi_redis::redis_com::qa_bo
}

node /^qa-bo-redis-streaming/ {
  include sentifi_redis::redis_streaming::qa_bo
}

node /^qa-bo-pgsql-rip/ {
  include tuning
  include sentifi_postgresql::postgresql_rip::qa_bo
}

node /^qa-bo-pgsql-da0/ {
  include tuning
  include sentifi_postgresql::postgresql_da0::qa_bo
}

node /^qa-bo-factset/ {
  include tuning
  include sentifi_postgresql::postgresql_factset::qa_bo
}

node /^qa-bo-topic-service/ {
  $db_url = $qa_rip_url
  $db_username = "ptopicservice"
  $db_password = "&@k#sPoeB"
  $elasticsearch_cluster_name = $qa_es_cluster
  $elasticsearch_cluster_nodes = $qa_es_host
  $gcloud_pubsub_project = "sentifi-backoffice-qa"
  $gcloud_pubsub_topic = "topic-cdc"

  include tuning
  include sentifi_topic_service::qa_bo
}

node /^qa-bo-profile-service/ {
  $db_url = $qa_rip_url
  $db_username = "pprofileservice"
  $db_password = "Z>[^]2#ik"
  $gcloud_pubsub_project = "sentifi-backoffice-qa"
  $gcloud_pubsub_topic = "profile-cdc"

  include tuning
  include sentifi_profile_service::qa_bo
}

node /^qa-bo-stock-price/ {
  $db_url = $qa_rip_url
  $db_username = "pstockpricefetch"
  $db_password = "*)hatQkD@"
  $topic_service_url = $qa_topic_service_url
  include tuning
  include sentifi_stock_price::qa_bo
}

node /^qa-bo-data-management-portal/ {
  $clientId = $google_oauth2_client_id
  $clientSecret = $google_oauth2_client_secret
  $db_url = $qa_rip_url
  $db_username = "pportalmgmt"
  $db_password = "5Z5S*1>{!"
  $topic_service_url = $qa_topic_service_url
  $profile_service_url = $qa_profile_service_url
  $version = $data_management_portal_qa_bo_version
  $elasticsearch_cluster_name = $qa_es_cluster
  $elasticsearch_cluster_nodes = $qa_es_host

  include sentifi_data_management_portal::qa_bo
}

#qa-bo-ext-es
node /^qa-bo-ext-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_ext::master
}

node /^qa-bo-ext-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_ext::data
}

node /^qa-bo-ext-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_ext::client
}

#qa-bo-int-es
node /^qa-bo-int-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_int::master
}

node /^qa-bo-int-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_int::data
}

node /^qa-bo-int-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_1::qa_bo_int::client
  include sentifi_logstash
}

# rubber
node /^qa-bo-rubber-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_rubber::rubber::master
}

node /^qa-bo-rubber-es-hot/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_rubber::rubber::hot
}

node /^qa-bo-rubber-es-cold/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_rubber::rubber::cold
}

node /^qa-bo-rubber-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_rubber::rubber::client
}

node /^qa-bo-rubber-es-kibana/ {
  include tuning
  include sentifi_kibana_rubber::qa_bo
}

#qa-bo-six-es
node /^qa-bo-six-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_6::qa_bo_six::master
}

node /^qa-bo-six-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_6::qa_bo_six::data
}

node /^qa-bo-six-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch_6::qa_bo_six::client
}

#qa-bo-sim-es
node /^qa-bo-sim-es-master/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::qa_bo_sim::master
}

node /^qa-bo-sim-es-data/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::qa_bo_sim::data
}

node /^qa-bo-sim-es-client/ {
  class {"tuning":
    elastic =>  true,
  }
  include sentifi_elasticsearch::qa_bo_sim::client
}

node /^qa-bo-nginx/ {
  include tuning
  include sentifi_nginx::qa_bo
  include sentifi_kibana::qa_bo
}

node /^qa-streaming-swarm-node/ {
  include stackdriver::logging::streaming_docker_container
}

node /^qa-bo-de-dev/ {
	include tuning
}


#---PROD FO NODES---
node /^prod-fo-backend-notification/ {
  $db_url = $prod_rip_url
  $db_host = $prod_rip_host
  $db_port = $prod_rip_port
  $db_name = $prod_rip_name
  $db_username = "pnotification"
  $db_password = "e^?5xG:im"
  $es_host = $prod_es_host
  $es_cluster = $prod_es_cluster
  $redis_host = $prod_redis_host
  $ably_token_published = $prod_notification_ably_token_published
  $aws_secretkey = $prod_notification_aws_secretkey
  $ably_token_subscribed = $prod_notification_ably_token_subscribed
  $jwt_token_secretkey = $prod_notification_jwt_token_secretkey
  $admin_password = "53nt1f1.V13tn4m"
  $internal_password = "53nt1f1.Int3rn4l"
  $aws_sns_apple_arn = $prod_notification_aws_sns_apple_arn
  $aws_sns_android_arn = $prod_notification_aws_sns_android_arn
  $apple_template_engine = "APNS"
  $event_publishing_schedule_delay = "60000"
  $event_publishing_schedule_rate = "60000"
  $thymeleaf_cache = "true"
  $oauth2_enabled = "true"
  include tuning
  include sentifi_backend_notification::prod_fo
}

node /^prod-fo-backend-workers/ {
  $db_host = $prod_rip_host
  $db_port = $prod_rip_port
  $db_name = $prod_rip_name
  $db_username = "pbackendapi"
  $db_password = "o(?,E~-A&"
  $ne_client_sercret = "notificationEngineRul3s!"
  $ne_root_url = $prod_api_url
  $email_queue_name = "PROD_WEBSITE_EMAIL"
  include tuning
  include sentifi_backend_worker::prod_fo
}

node /^prod-fo-restapi/ {
  include tuning
  include sentifi_restapi::prod_fo
}

node /^prod-fo-api/ {
  include tuning
  include sentifi_restapi::api::prod_fo
}

node /^prod-fo-nginx/ {
  include tuning
  include sentifi_nginx::prod_fo
  include sentifi_varnish::prod_fo
}

node /^prod-fo-varnish/ {
  include tuning
  include sentifi_varnish::prod_fo_15m
}

node /^prod-fo-website/ {
  include tuning
  include sentifi_website::prod_fo
}

node /^prod-fo-widget-service/ {
  include tuning
  include sentifi_widget_service::prod_fo
}

#---QA FO NODES---

node /^qa-fo-nginx/ {
  include tuning
  include sentifi_nginx::qa_fo
}

node /^qa-fo-restapi/ {
  include tuning
  include sentifi_restapi::qa_fo
}

node /^qa-fo-website/ {
  include tuning
  include sentifi_website::qa_fo
}

node /^qa-fo-backend-workers/ {
  $db_host = $qa_rip_host
  $db_port = $qa_rip_port
  $db_name = $qa_rip_name
  $db_username = "pbackendapi"
  $db_password = "o(?,E~-A&"
  $ne_client_sercret = "notificationEngineRul3s!"
  $ne_root_url = $qa_api_url
  $email_queue_name = "QC_WEBSITE_EMAIL"
  include tuning
  include sentifi_backend_worker::qa_fo
}

node /^qa-fo-backend-notification/ {
  $db_url = $qa_rip_url
  $db_host = $qa_rip_host
  $db_port = $qa_rip_port
  $db_name = $qa_rip_name
  $db_username = "pnotification"
  $db_password = "e^?5xG:im"
  $es_host = $qa_es_host
  $es_cluster = $qa_es_cluster
  $redis_host = $qa_redis_host
  $ably_token_published = $qa_notification_ably_token_published
  $aws_secretkey = $qa_notification_aws_secretkey
  $ably_token_subscribed = $qa_notification_ably_token_subscribed
  $jwt_token_secretkey = $qa_notification_jwt_token_secretkey
  $admin_password = "Abcd1234"
  $internal_password = "Sentifi.Internal"
  $aws_sns_apple_arn = $qa_notification_aws_sns_apple_arn
  $aws_sns_android_arn = $qa_notification_aws_sns_android_arn
  $apple_template_engine = "APNS"
  $event_publishing_schedule_delay = "5256000000"
  $event_publishing_schedule_rate = "5256000000"
  $thymeleaf_cache = "false"
  $oauth2_enabled = "true"
  include tuning
  include sentifi_backend_notification::qa_fo
}

node /^qa-fo-widget-service/ {
  $redis_host = $qa_redis_host
  $db_url = $qa_rip_url
  $db_username = "pwidgetservice"
  $db_password = "3h--dz+Q,"
  $show_sql = "false"
  $hibernate_show_sql = "false"
  $es_host = $qa_es_host
  $es_cluster = $qa_es_cluster
  $sqs_queue_name = "QC_WEBSITE_EMAIL"
  $notification_engine_host = $qa_notification_engine_host
  $notification_engine_port = "80"
  $notification_engine_username = "internal"
  $notification_engine_password = "Sentifi.Internal"
  include tuning
  include sentifi_widget_service::qa_fo
}

#---INT NODES---
node /^int-fo-redis-com/ {
  include sentifi_redis::redis_com::int_fo
}

node /^int-fo-nginx/ {
  include tuning
  include sentifi_nginx::int_fo
}

node /^int-fo-restapi/ {
  include tuning
  include sentifi_restapi::int_fo
}

node /^int-fo-website/ {
  include tuning
  include sentifi_website::int_fo
}

node /^int-fo-pgsql-rip/ {
  include tuning
  include sentifi_postgresql::postgresql_rip::int_fo
}

node /^int-fo-widget-service/ {
  $redis_host = $int_redis_host
  $db_url = $int_rip_url
  $db_username = "pwidgetservice"
  $db_password = "3h--dz+Q,"
  $show_sql = "true"
  $hibernate_show_sql = "true"
  $es_host = $qa_es_host
  $es_cluster = $qa_es_cluster
  $sqs_queue_name = "DEV_WEBSITE_EMAIL"
  $notification_engine_host = $qa_notification_engine_host
  $notification_engine_port = "80"
  $notification_engine_username = "internal"
  $notification_engine_password = "Sentifi.Internal"
  include tuning
  include sentifi_widget_service::int_fo
}
