define sentifi_postgresql::wale (
  $module = "sentifi_postgresql",
  $pguser = "postgres",
  $pggroup = "postgres",
  $wale_gs_prefix = undef,
  $google_service_account_key = undef,
) {
  file {["/etc/wal-e.d", "/etc/wal-e.d/env"]:
    ensure => directory,
    owner  => root,
    group  => $pggroup,
  }->
  file {"/etc/wal-e.d/env/WALE_GS_PREFIX":
    ensure  => file,
    content => template("$module/wale/WALE_GS_PREFIX.pp"),
    owner   => root,
    group   => $pggroup,
  }->
  file {"/etc/wal-e.d/env/GOOGLE_APPLICATION_CREDENTIALS":
    ensure  => file,
    content => template("$module/wale/GOOGLE_APPLICATION_CREDENTIALS.pp"),
    owner   => root,
    group   => $pggroup,
  }->
  file {"/etc/wal-e.d/env/.$google_service_account_key":
    ensure => file,
    source => "puppet:///modules/$module/wale/$google_service_account_key",
    owner  => root,
    group  => $pggroup,
  }->
  package {["python34", "python34-pip", "lzop"]:
    ensure => installed,
  }->
  exec {"upgrade pip":
    command     => "python3 -m pip install --upgrade pip && python3 -m pip install --upgrade setuptools && python3 -m pip install virtualenv && python3 -m pip install wal-e[google] && python3 -m install protobuf==3.1.0",
    provider    => "shell",
    refreshonly => true,
    #subscribe  => Package["python34", "python34-pip"],
    creates     => "/bin/wal-e",
  }->
  file {"/data/daemontools-0.76.tar.gz":
    ensure => present,
    source => "puppet:///modules/$module/wale/daemontools-0.76.tar.gz",
  }->
  exec {"Decompress package daemontools on $package_name":
    command  => "tar -zxf daemontools-0.76.tar.gz",
    cwd      => "/data",
    provider => "shell",
    creates  => "/data/admin/daemontools-0.76",
  }->
  exec {"Fix error when installing daemontools on CentOS":
    command     => "echo gcc -O2 -include /usr/include/errno.h > /data/admin/daemontools-0.76/src/conf-cc",
    cwd         => "/data",
    provider    => "shell",
    subscribe   => Exec["Decompress package daemontools on $package_name"],
    refreshonly => true,
  }->
  exec {"Install daemontools on $package_name":
    command     => "./package/install",
    cwd         => "/data/admin/daemontools-0.76/",
    provider    => "shell",
    creates     => "/data/admin/daemontools/command/envdir",
  }->
  file {"/command/envdir":
    ensure => link,
    target => "/data/admin/daemontools/command/envdir",
    force  => true,
  }->
  file {"/usr/local/bin/envdir":
    ensure => link,
    target => "/command/envdir",
    force  => true,
  }->
  file {"/etc/wal-e.d/push_wal_files.sh":
    ensure => file,
    source => "puppet:///modules/$module/wale/push_wal_files.sh",
    mode   => "0755",
    owner  => root,
    group  => $pggroup,
  }
}
