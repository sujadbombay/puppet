class sentifi_postgresql::postgresql_zabbix {
  sentifi_postgresql::init {"postgresql_zabbix":
    databases => [ 'postgres', 'zabbix' ],
  }
  sentifi_postgresql::postgresql_users {"zabbix":
    pguser_pw => "+ayWb)fhP",
  }
}

class sentifi_postgresql::postgresql_zabbix::prod_ops inherits sentifi_postgresql::postgresql_zabbix {
  Sentifi_postgresql::Init["postgresql_zabbix"]{
    shared_buffers       => "2560MB",  # 1/4 of total RAM
    effective_cache_size => "5GB",  # 1/2 of total RAM
  }
}
