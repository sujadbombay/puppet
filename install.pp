define sentifi_elasticsearch_6::install (
  $package_name = $title,
  $es_master_nodes = undef,
  $data_path = "/data/elasticsearch",
  $log_path = "/data/eslog",
  $cluster_name = undef,
  $es_heap_size = undef,
  $master = false,
  $data = true,
  $module = "sentifi_elasticsearch_6",
  $buffer_size = "45%",
  $project_id = undef,
  $zone = undef,
  $tags = undef,
  $type = "gce", 
  $min_master_nodes = "1",
  $cache_size = "30%",
  $temp_dir = "/data/temp",
  $es_version = latest,
) {
  $discovery_hosts = $es_master_nodes
  include stackdriver::elasticsearch
  class {"stackdriver::logging::elasticsearch":
    cluster_name =>  $cluster_name,
  }->
  sentifi_java::centos7 {"$package_name":
    java_version =>  "1.8.0_121",
  }->
  exec {"Import Elastic Search Key for $package_name $hostname":
    command  => "rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch",
    unless   => "rpm -qa --nodigest --nosignature --qf '%{VERSION}-%{RELEASE} %{SUMMARY}\n' | grep d88e42b4",
    provider => "shell",
  }->
  yumrepo {"elasticsearch":
    name     => "elasticsearch-6.x",
    baseurl  => "https://artifacts.elastic.co/packages/6.x/yum",
    descr    => "Elasticsearch repository for 6.x packages",
    enabled  => "1",
    gpgcheck => "1",
    gpgkey   => "https://artifacts.elastic.co/GPG-KEY-elasticsearch",
  }->
  package {"elasticsearch":
    ensure => "$es_version",
  }->
  exec {"Create elasticsearch data dir $data_path for $package_name $hostname":
    command  => "mkdir -p $data_path && chown -R elasticsearch: $data_path",
    provider => "shell",
    creates  => $data_path,
  }->
  exec {"Create elasticsearch log dir $log_path for $package_name $hostname":
    command  => "mkdir -p $log_path && chown -R elasticsearch: $log_path",
    provider => "shell",
    creates  => $log_path,
  }->
  exec {"Create elasticsearch temp dir $temp_dir for $package_name $hostname":
    command  => "mkdir -p $temp_dir && chown -R elasticsearch: $temp_dir",
    provider => "shell",
    creates  => $temp_dir,
  }->
  file {"/var/log/elasticsearch":
    ensure  => link,
    target  => $log_path,
    force   => true,
    owner   => "elasticsearch",
    group   => "elasticsearch",
    require => Exec["Create elasticsearch log dir $log_path for $package_name $hostname"],
  }->
  file {["$data_path","$log_path"]:
    ensure => directory,
    owner  => "elasticsearch",
    group  => "elasticsearch",
  }->
  file_line {"Fix elasticsearch systemd":
    path  => "/usr/lib/systemd/system/elasticsearch.service",
    line  => "LimitMEMLOCK=infinity",
    after => "Service",
  }->
  file {"/etc/elasticsearch/elasticsearch.yml":
    ensure  => present,
    content => template("$module/elasticsearch.yml.erb"),
    require => Package["elasticsearch"],
  }->
  file {"/etc/elasticsearch/jvm.options":
    ensure  => present,
    content => template("$module/jvm.options.erb"),
    require => Package["elasticsearch"],
  }->
  file {"/etc/sysconfig/elasticsearch":
    ensure  => present,
    content => template("$module/elasticsearch.erb"),
  }->
  exec {"Reload Daemon Elasticsearch Service":
    command     => "systemctl daemon-reload",
    provider    => "shell",
    refreshonly => true,
    subscribe   => File_line["Fix elasticsearch systemd"],
  }->
  exec {"check elasticsearch plugin discovery-gce for $package_name":
    command  => "bin/elasticsearch-plugin remove discovery-gce",
    provider => "shell",
    onlyif   => "bin/elasticsearch-plugin list | grep discovery-gce | egrep 'incompatible|WARNING'",
    cwd      => "/usr/share/elasticsearch",
  }->
	exec {"check elasticsearch plugin x-pack for $package_name":
    command  => "bin/elasticsearch-plugin remove x-pack",
    provider => "shell",
    onlyif   => "bin/elasticsearch-plugin list | grep x-pack | egrep 'incompatible|WARNING'",
    cwd      => "/usr/share/elasticsearch",
  }->
	exec {"check elasticsearch plugin sql for $package_name":
    command  => "bin/elasticsearch-plugin remove sql",
    provider => "shell",
    onlyif   => "bin/elasticsearch-plugin list | grep sql | egrep 'incompatible|WARNING'",
    cwd      => "/usr/share/elasticsearch",
  }->
	exec {"Install elasticsearch plugin discovery-gce for $package_name":
    command  => "echo y | bin/elasticsearch-plugin install discovery-gce",
    provider => "shell",
    unless   => "bin/elasticsearch-plugin list | grep discovery-gce",
    cwd      => "/usr/share/elasticsearch",
  }->

# Elasticsearch 6 contains X-Pack by default
#  exec {"Install elasticsearch plugin xpack for $package_name":
#    command  => "bin/elasticsearch-plugin install x-pack",
#    provider => "shell",
#    unless   => "bin/elasticsearch-plugin list | grep x-pack",
#    cwd      => "/usr/share/elasticsearch",
#  }->
  service {"elasticsearch":
    ensure => running,
    enable => true,
  }
#  exec {"Restart Elasticsearch Service":
#    command     => "systemctl restart elasticsearch",
#    provider    => "shell",
#    refreshonly => true,
#    subscribe   => [File["/etc/elasticsearch/elasticsearch.yml","/etc/elasticsearch/jvm.options","/etc/sysconfig/elasticsearch"],Exec["Install elasticsearch plugin discovery-gce for $package_name"]],
#  }
}
