define sentifi_postgresql::init (
  $package_name = $title,
  $module = "sentifi_postgresql",
  $mount_point_sdb = "/data/postgresql",
  $pguser = "postgres",
  $pggroup = "postgres",
  $pgver = "9.6",
  $service = "postgresql-$pgver",
  $pgpackage_ver = "96",
  $shared_buffers = "1792MB",  # 1/4 of total RAM
  $effective_cache_size = "3584MB",  # 1/2 of total RAM
  $max_wal_size = "512",
  $repl = false,
  $slave = false,
  $env = undef,
  Array $databases = [ 'postgres' ],
) {
  sentifi_disk {"pgsql":
    device      => "sdb",
    mount_point => $mount_point_sdb,
  }->
  file {"$mount_point_sdb/pgsql":
    ensure => directory,
  }->
  file {"/var/lib/pgsql":
    ensure => link,
    target => "$mount_point_sdb/pgsql",
    force  => true,
  }->
  yumrepo {"postgresql-$pgver":
    name     => "postgresql-$pgver",
    baseurl  => "https://download.postgresql.org/pub/repos/yum/$pgver/redhat/rhel-7-x86_64",
    descr    => "PostgreSQL $pgver repo for CentOS 7 x86_64",
    enabled  => "1",
    gpgcheck => "0",
  }->
  yumrepo {"pgpool$mainver":
    name     => "pgpool$mainver",
    baseurl  => "http://www.pgpool.net/yum/rpms/3.6/redhat/rhel-7-x86_64",
    descr    => "pgpool-II 3.6 repo for CentOS 7 x86_64",
    enabled  => "1",
    gpgcheck =>   "0",
  }->
  package {["postgresql$pgpackage_ver-contrib", "postgresql$pgpackage_ver-server", "postgresql$pgpackage_ver", "postgresql$pgpackage_ver-libs", "repmgr$pgpackage_ver", "pgpool-II-pg$pgpackage_ver-extensions"]:
    ensure => installed,
  }->
  file {["/var/lib/pgsql/logs", "/var/lib/pgsql/scripts", "/var/lib/pgsql/backup", "/var/lib/pgsql/backup/backup_logs", "/var/run/postgresql", "/data/scripts", "/data/scripts/logs"]:
    ensure => directory,
    owner  => $pguser,
    group  => $pggroup,
  }->
  file {"/etc/profile.d/postgres.sh":
    ensure  => file,
    content => template("$module/postgres_profile.sh.erb"),
  }->
  exec {"Reload profile":
    command     => ". /etc/profile",
    provider    => "shell",
    refreshonly => true,
    subscribe   => File["/etc/profile.d/postgres.sh"],
  }->
  exec {"Initialize PostgreSQL database":
    command  => "/usr/pgsql-$pgver/bin/postgresql$pgpackage_ver-setup initdb",
    provider => "shell",
    creates  => "/var/lib/pgsql/$pgver/data",
  }->
  file {"/var/lib/pgsql/$pgver/data/postgresql.conf":
    ensure  => file,
    content => template("$module/$package_name/postgresql.conf.erb"),
    owner   => $pguser,
    group   => $pggroup,
    backup  => true,
  }->
  file {"/var/lib/pgsql/$pgver/data/pg_hba.conf":
    ensure  => present,
    content => template("$module/$package_name/pg_hba.conf.erb"),
    owner   => $pguser,
    group   => $pggroup,
    backup  => true,
  }->
  class {"stackdriver::postgresql":
    databases => $databases,
  }->
  service {"$service":
    ensure => running,
    enable => true,
  }->
  exec {"Restart $service":
    command     => "systemctl restart $service",
    provider    => "shell",
    refreshonly => true,
    subscribe   => File["/var/lib/pgsql/$pgver/data/postgresql.conf"],
  }->
  exec {"Reload $service":
    command     => "systemctl reload $service",
    provider    => "shell",
    refreshonly => true,
    subscribe   => File["/var/lib/pgsql/$pgver/data/pg_hba.conf"],
  }->
  sentifi_postgresql::postgresql_users {"psdagent":
    pguser_pw => "s.BFh=R.o",
  }
}
