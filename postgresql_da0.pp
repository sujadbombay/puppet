class sentifi_postgresql::postgresql_da0 (
  Array $ddllogroles = [ 'pnewcandmatch' ],
) {
  sentifi_postgresql::init {"postgresql_da0":
    databases => [ 'postgres', 'da0', 'geo_names', 'ml' ],
  }
  $ddllogroles.each |String $ddllogrole| {
    exec {"Set log level ddl for user ${ddllogrole}":
      command  => "psql -c \'ALTER ROLE ${ddllogrole} SET log_statement = ddl\'",
      user     => "postgres",
      provider => "shell",
      cwd      => "/var/lib/pgsql",
      unless   =>   "psql -c \"select rolconfig from pg_roles where rolname = \'${ddllogrole}\'\" | grep \'log_statement=ddl\'";
    }
  }
#  sentifi_postgresql::postgresql_users {"dbo":
#    pguser_pw => "sentifi",
#  }
#  sentifi_postgresql::postgresql_users {"dbw":
#    pguser_pw => "sentifi",
#  }
#  sentifi_postgresql::postgresql_users {"plocationdetection":
#    pguser_pw => "Uathi6ah",
#  }
#  sentifi_postgresql::postgresql_users {"ppiprocess":
#    pguser_pw => "Zoqu0Zah",
#  }
#  sentifi_postgresql::postgresql_users {"ppushertopiprocess":
#    pguser_pw => "aiPah3ee",
#  }
#  sentifi_postgresql::postgresql_users {"ptwitterprofilecrawler":
#    pguser_pw => "Jai4yait",
#  }
#  sentifi_postgresql::postgresql_users {"uadmin":
#    pguser_pw => "Kilei6Ne",
#  }
#  sentifi_postgresql::postgresql_users {"uengsupport":
#    pguser_pw => "oeXe7aeJ",
#  }
}

class sentifi_postgresql::postgresql_da0::qa_bo inherits sentifi_postgresql::postgresql_da0 {
  Sentifi_postgresql::Init["postgresql_da0"]{
    shared_buffers       => "1GB",  # 1/4 of total RAM
    effective_cache_size => "5GB",  # 1/2 of total RAM
  }
}

class sentifi_postgresql::postgresql_da0::prod_bo (
  $package_name = "postgresql_da0",
  $module = "sentifi_postgresql",
) inherits sentifi_postgresql::postgresql_da0 {
  Sentifi_postgresql::Init["postgresql_da0"]{
    shared_buffers       => "12GB",  # 1/4 of total RAM
    effective_cache_size => "40GB",  # 1/2 of total RAM
  }->
  file {"/data/scripts/prod_da0_google_snapshots_weekly.sh":
    ensure => file,
    source => "puppet:///modules/$module/$package_name/prod_da0_google_snapshots_weekly.sh",
    owner  => "root",
    group  => "root",
  }->
  cron {"Daily backup using Google Disk Snapshot":
    command => "sh /data/scripts/prod_da0_google_snapshots_weekly.sh > /data/scripts/logs/prod_da0_google_snapshots_weekly_`date '+\%y\%m\%d-\%H\%M'`.log 2>&1",
    user    => "postgres",
    hour    => "19",
    minute  => "01",
    weekday => "01",
  }
}
