class sentifi_elasticsearch_6::rubber::master {
  sentifi_elasticsearch_6::install {"rubber":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "rubber",
    es_heap_size     => "1g",
    master           => true,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "rubber",
    min_master_nodes => "2",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
} 

class sentifi_elasticsearch_6::rubber::hot {
  sentifi_disk {"rubber":
    device      => "sdb",
    mount_point =>  "/data",
  }->
  sentifi_elasticsearch_6::install {"rubber":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "rubber",
    es_heap_size     => "6g",
    master           => false,
    data             => true,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "rubber",
    min_master_nodes => "2",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
}

class sentifi_elasticsearch_6::rubber::cold {
  sentifi_disk {"rubber":
    device      => "sdb",
    mount_point =>  "/data",
  }->
  sentifi_elasticsearch_6::install {"rubber":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "rubber",
    es_heap_size     => "13g",
    master           => false,
    data             => true,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "rubber",
    min_master_nodes => "2",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
}

class sentifi_elasticsearch_6::rubber::client {
  sentifi_elasticsearch_6::install {"rubber":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "rubber",
    es_heap_size     => "1g",
    master           => false,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "rubber",
    min_master_nodes => "2",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
}
