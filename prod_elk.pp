class sentifi_elasticsearch_6::prod_elk {
  sentifi_disk {"prod-elk":
    device      => "sdb",
    mount_point =>  "/data",
  }->
  sentifi_elasticsearch_6::install {"prod-elk":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "prod-elk",
    es_heap_size     => "16g",
    master           => true,
    data             => true,
    project_id       => "sentifi-operations-prod",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "prod-elk",
    min_master_nodes => "1",
    buffer_size      => "15%",
    cache_size       => "15%",
  }
}

class sentifi_elasticsearch_6::prod_elk::master {
  sentifi_elasticsearch_6::install {"prod-elk":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "prod-elk",
    es_heap_size     => "2g",
    master           => true,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "prod-elk",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
} 

class sentifi_elasticsearch_6::prod_elk::data {
  sentifi_disk {"prod-sim-es":
    device      => "sdb",
    mount_point =>  "/data",
  }->
  sentifi_elasticsearch_6::install {"prod-elk":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "prod-elk",
    es_heap_size     => "18g",
    master           => false,
    data             => true,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "prod-elk",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
} 

class sentifi_elasticsearch_6::prod_elk::client {
  sentifi_elasticsearch_6::install {"prod-elk":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "prod-elk",
    es_heap_size     => "9g",
    master           => false,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "prod-elk",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
}
