class sentifi_postgresql::postgresql_factset {
  sentifi_postgresql::init {"postgresql_factset":
    databases => [ 'postgres', 'fds' ],
  }
}

class sentifi_postgresql::postgresql_factset::qa_bo inherits sentifi_postgresql::postgresql_factset {
  Sentifi_postgresql::Init["postgresql_factset"]{
    shared_buffers       => "2560MB",  # 1/4 of total RAM
    effective_cache_size => "5GB",  # 1/2 of total RAM
  }
}
