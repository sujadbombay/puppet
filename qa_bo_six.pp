class sentifi_elasticsearch_6::qa_bo_six::master {
  sentifi_elasticsearch_6::install {"qa-bo-six-es":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "qa-six-es",
    es_heap_size     => "2g",
    master           => true,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "qa-six-es",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
} 

class sentifi_elasticsearch_6::qa_bo_six::data {
  sentifi_disk {"qa-bo-six":
    device      => "sdb",
    mount_point =>  "/data",
  }->
  sentifi_elasticsearch_6::install {"qa-bo-six-es":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "qa-six-es",
    es_heap_size     => "18g",
    master           => false,
    data             => true,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "qa-six-es",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
} 

class sentifi_elasticsearch_6::qa_bo_six::client {
  sentifi_elasticsearch_6::install {"qa-bo-six-es":
    data_path        => "/data/elasticsearch",
    log_path         => "/data/eslog",
    cluster_name     => "qa-six-es",
    es_heap_size     => "9g",
    master           => false,
    data             => false,
    project_id       => "sentifi-backoffice-qa",
    zone             => "europe-west1-b",
    type             => "gce",
    tags             => "qa-six-es",
    min_master_nodes => "1",
    buffer_size      => "45%",
    cache_size       => "30%",
  }
}
